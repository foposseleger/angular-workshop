import { Component, OnInit } from '@angular/core';
import { FaceSnap } from './models/face-snap.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  faceSnaps!: FaceSnap[];


  ngOnInit(): void {
    this.faceSnaps = [
     {
        title: 'Corrine',
        description: 'Ma belle épouse',
        imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
        createdDate: new Date(),
        snaps: 250,
        location: 'Paris'
    },
    {
      title: 'Corrine',
      description: 'Ma belle épouse',
      imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
      createdDate: new Date(),
      snaps: 0
   },
   {
    title: 'Corrine',
    description: 'Ma belle épouse',
    imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
    createdDate: new Date(),
    snaps: 0,
    location: 'Montreal'
   },
     {
        title: 'Corrine',
        description: 'Ma belle épouse',
        imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
        createdDate: new Date(),
        snaps: 0,
        location: 'Paris'
    },
    {
      title: 'Corrine',
      description: 'Ma belle épouse',
      imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
      createdDate: new Date(),
      snaps: 0
   },
   {
    title: 'Corrine',
    description: 'Ma belle épouse',
    imageUrl: 'https://www.avisgolf.com/wp-content/uploads/2019/07/Michelle-Wie-main-2.jpg',
    createdDate: new Date(),
    snaps: 0,
    location: 'Montreal'
   }
  ]
    }
}
