import { Component, OnInit, Input } from '@angular/core';
import { FaceSnap } from '../models/face-snap.model';

@Component({
  selector: 'app-face-snap',
  templateUrl: './face-snap.component.html',
  styleUrls: ['./face-snap.component.scss']
})
export class FaceSnapComponent implements OnInit {
  @Input() faceSnap!: FaceSnap; 
  isSnap!: Boolean;
  buttonLabel!: String;
  

  ngOnInit(): void {
    this.buttonLabel = 'Oh Snap!';
    this.isSnap = false;

  }

  onSnap(): void {
    if (!this.isSnap) {
      this.buttonLabel = 'Oops, unSnap!'
      this.faceSnap.snaps++;
      this.isSnap = true;
    } else {
      this.buttonLabel = 'Oh snap!'
      this.faceSnap.snaps--;
      this.isSnap = false;
    }
    
  }
}
